import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { CreateTransactionDto } from 'src/dtos/create-transaction.dto';
import { UpdateTransactionDto } from 'src/dtos/update-transaction.dto';
import { TransactionService } from 'src/services/transaction/transaction.service';

@Controller('transaction')
export class TransactionController {
  constructor(private readonly transactionService: TransactionService) {}

  @Post()
  async createTransaction(
    @Res() response,
    @Body() createTransactionDto: CreateTransactionDto,
  ) {
    try {
      const newTransaction = await this.transactionService.createTransaction(
        createTransactionDto,
      );
      return response.status(HttpStatus.CREATED).json({
        message: 'Transaction has been created successfully',
        newTransaction,
      });
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: 'Error: Transaction not created!',
        error: 'Bad Request',
      });
    }
  }

  @Put('/:id')
  async updateTransaction(
    @Res() response,
    @Param('id') transactionId: string,
    @Body() updateTransactionDto: UpdateTransactionDto,
  ) {
    try {
      const existingTransaction =
        await this.transactionService.updateTransaction(
          transactionId,
          updateTransactionDto,
        );
      return response.status(HttpStatus.OK).json({
        message: 'Transaction has been successfully updated',
        existingTransaction,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get()
  async getTransactions(@Res() response) {
    try {
      const transactionData =
        await this.transactionService.getAllTransactions();
      return response.status(HttpStatus.OK).json({
        message: 'All transactions data found successfully',
        transactionData,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Get('/:id')
  async getTransaction(@Res() response, @Param('id') transactionId: string) {
    try {
      const existingTransaction = await this.transactionService.getTransaction(
        transactionId,
      );
      return response.status(HttpStatus.OK).json({
        message: 'Transaction found successfully',
        existingTransaction,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }

  @Delete('/:id')
  async deleteTransaction(@Res() response, @Param('id') transactionId: string) {
    try {
      const deletedTransaction =
        await this.transactionService.deleteTransaction(transactionId);
      return response.status(HttpStatus.OK).json({
        message: 'Transaction deleted successfully',
        deletedTransaction,
      });
    } catch (err) {
      return response.status(err.status).json(err.response);
    }
  }
}
