import { Document } from 'mongoose';

export interface ITransaction extends Document {
  readonly id: string;
  readonly studentName: string;
}
