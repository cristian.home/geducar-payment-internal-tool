import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateTransactionDto } from 'src/dtos/create-transaction.dto';
import { UpdateTransactionDto } from 'src/dtos/update-transaction.dto';
import { ITransaction } from 'src/interfaces/transaction.interface';
import { Model } from 'mongoose';

@Injectable()
export class TransactionService {
  constructor(
    @InjectModel('Transaction') private transactionModel: Model<ITransaction>,
  ) {}

  async createTransaction(
    createTransactionDto: CreateTransactionDto,
  ): Promise<ITransaction> {
    const newTransaction = await new this.transactionModel(
      createTransactionDto,
    );
    return newTransaction.save();
  }

  async updateTransaction(
    transactionId: string,
    updateTransactionDto: UpdateTransactionDto,
  ): Promise<ITransaction> {
    const existingTransaction = await this.transactionModel.findByIdAndUpdate(
      transactionId,
      updateTransactionDto,
      { new: true },
    );

    if (!existingTransaction) {
      throw new NotFoundException(`Transaction #${transactionId} not found`);
    }

    return existingTransaction;
  }

  async getAllTransactions(): Promise<ITransaction[]> {
    const transactionData = await this.transactionModel.find();
    if (!transactionData || transactionData.length == 0) {
      throw new NotFoundException('Transactions data not found!');
    }
    return transactionData;
  }

  async getTransaction(transactionId: string): Promise<ITransaction> {
    const existingTransaction = await this.transactionModel
      .findById(transactionId)
      .exec();
    if (!existingTransaction) {
      throw new NotFoundException(`Transaction #${transactionId} not found`);
    }
    return existingTransaction;
  }

  async deleteTransaction(transactionId: string): Promise<ITransaction> {
    const deletedTransaction = await this.transactionModel.findByIdAndDelete(
      transactionId,
    );
    if (!deletedTransaction) {
      throw new NotFoundException(`Transaction #${transactionId} not found`);
    }
    return deletedTransaction;
  }
}
