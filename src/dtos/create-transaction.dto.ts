import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class CreateTransactionDto {
  @IsString()
  @MaxLength(30)
  @IsNotEmpty()
  readonly id: string;

  @IsString()
  @IsNotEmpty()
  readonly studentName: string;
}
